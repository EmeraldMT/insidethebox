
Inside The Box
==============

A multi-player puzzle solving and creation game for minetest.

The game is meant to be run as a multiplayer puzzle-creating server.
Players can connect and either play boxes, or create new ones.

The basic concept of the game is boxes. This is an area in which the
player needs to find one or more objectives (keys). The player then
needs to bring these objectives to a slot (keyhole). Once all the
secrets have been found and returned, an exit opens that allows the
player out, completing the puzzle box.

This minetest game is strongly inspired by the "parcels" MC player
map. I did not want to replicate the map itself, even though the
conversion tools exist to mostly do this, but more because giving
it a multiplayer spin sounds much more appealing, and a far better
tribute to the map itself. The possibility of having hundreds of
puzzle boxes created also sounds much more appealing.


## Playing

A player can choose to play available boxes. Selecting an available
level to play will create a private copy of the level box and teleport
the player to the starting point of the level. The starting point is
in a hallway or entry room. There can be some clues left, and there
should be some information of the builder, and perhaps difficulty,
time spent by players in the level, popularity, rating information.

From the hallway the player should have some visual on the inside of
the box. This will help players recognize boxes easier so they can
skip ones they have played before, or gauge difficulty, etc..

The player moves through a door to the level. This may trigger a
teleport to a starting location, or not. On the side of the wall
somewhere is displayed a large number corresponding with the box ID.

In the box there is a collection area. In the collection area there
are 1 or more collection slots. The slots all require to be filled
with a "key" item. These items are found anywhere in the level. The
player picks up the item and brings them to the collection slots.

The box largely contains unbreakable nodes. The box may contain certain
digging tools to break nodes in the box (e.g. an axe to break certain
wood blocks, a pickaxe to break stone blocks, or a shovel to dig
sand blocks).  The player may be forced to use these tools to reach
new areas in the box, or to create bridges or ladders to climb up.

The player can not physically exit the box. The box is surrounded by
unbreakable blocks uncluding invisible ceilings where needed. The
player has an "escape" button that, when used, removes the player
from the game and returns the player to the general lobby.

The player may take damage in the box through falling or lava
or cacti, or other methods. The player recovers lost health somewhat
quickly, but some errors may be deadly.

Once the player collects all the needed items, an exit doorway opens
or becomes reachable. The player moves through the exit doorway and
ends in an exit lobby/hallway.

In the exit lobby/hallway, the player can leave feedback or ranking
votes for the level. Last, there is a method for the player to return
to the general lobby. The player also receives a ranking/score
about their performance in the box itself, but only on the first
attempt. The performance includes time spent in the box, times died,
and damage taken.


## Constructing

Players can construct new boxes. They do this by entering a pristine
box that is created on the fly for them. In this box, the player
receives full editing privileges. The player can construct and modify
the collection point, place the secrets, place any node or liquid,
fire, TNT or other element provided to them.

The entry hallway has a spot for a sign hint that the constructor can
modify. It also has an interface for modifying the box parameters,
such as name and size, and variations such as entry/exit gate locations
and collection point stations.


## Points

Points act as a measure of commitment. The more actions a player does
that are in the interest of the community, the more points a player
gains. These points can be exchanged for privileges.

For each level played and rated the player earns points.

A player may lose points for dying a lot in boxes, or dying a lot
in boxes in general. A player may also lose points for other actions,
such as going idle in a box, never rating boxes or always rating
boxes the same level of hearts. This lowers the input their
actions they have into the system. E.g. if a player continuously
dies many times in a few boxes, their scoring values are reduced
in the averages by a factor, reducing their impact on ratings.

Privileges that can be reached through gaining points are:
- ability to create new boxes
- ability to submit more boxes for trials
- ability to choose larger level boxes
- moderation of boxes that are eligible for moderation
- moderation of other players with a lower point level


## Ranking/Rating

Boxes are rated/ranked per players. Good boxes are promoted to box
series that can be accessed from the main lobby easily ("play this
months' top boxes") and ultimately make it in to a list of ultimate
box boxes. This level series of boxes will only ever grow, and the
player can track his progress through these series and continue at
any point left.

A Random box can be chosen from the lobby.

A player can test trial boxes. Trial boxes are not yet finished
but submitted by the creator for review. The player can rate and
provide comments ("can't continue", "fell and stuck in a hole",
"too easy") to the creator so they may modify the box.

Points are scored for time spent. More time equals more points, except
when players are leaving before finishing, which subtracts points.

Points are scored for player rating. 0-5 hearts can be given.

Points are scored for damage taken by players, but subtracted for
multiple deaths of the same player. In general, a player dying once
in a level is OK, as long as it is instructional to the player and
they understand afterwards how to overcome an obstacle. Subsequent
deaths of a player may subtract points (note: need to account for
trolling here).


## Series

Series are curated boxes that can be played in succession. A
series of boxes grows one box at a time (interval to be
determined) and the newest box is always the top ranked box that is
not yet in that series. It may be that a fixed amount of scoring points
needs to be gained before a box can be promoted to a series.

- Master Series: A series of best rated boxes over time. These are
boxes that have exceeded a certain amount of points. They are
permanently added to the master series forever.

- The trial Series: Unranked and unfinished boxes. These are marked
"can be played" but "review requested". Players can recieve extra
points when they play and rank these.

- The Newest Series: Best ranked boxes in a certain time frame. These
are up and coming boxes that are relatively new, but have been past
review already (no longer "review requested"). Players are encouraged
to play these boxes every week and rate.


## Other box choices

These are suggested game play modes to vary game play a bit.

- Random rated or Random unrated box: Play a random box.
- Box Race: Several players each attempt the same box separately.
- Coop: Try boxes together with another player.


## Roles

- Admin: reserved role for a player that can always moderate players
and/or boxes, ban/kick players, edit all boxes, remove boxes.
- Moderator: may suspend players, may suspend boxes.
- Player: no privileges.


## Technology

Inside The Box uses lsqlite3 to store player, box, series and score
data. This assures that large amounts of data can be stored easily
with reasonably good performance and reliability.

For creation and playing of maps, a virtual `plot` is reserved per
player in a random location on the map away from the static lobby. This
reservation is destroyed when the box is no longer needed.

The box contents are stored in the sqlite database and restored for
each player that attempts to play the box. This assures that multiple
players can separately try the same box without conflicts.

The sqlite backend also allows efficient storing and retrieval of
scores for players or boxes and creating ranks, modifying or inspecting
state of boxes or players.

Placement of boxes will use VoxelManips as well as additional metadata
where needed to connect dynamic elements inside the box. These are
subsequently connected to assure proper functioning of the box.


## Running this game

 1. Install insidethebox in the `games` folder like a normal game.

 2. Install `luarocks` for Lua 5.1. Your distribution most likely will
    not have it - you'll have to compile it from source code. You
    can find instructions on how to do this for instance here:
      - https://stackoverflow.com/questions/20321560/how-do-install-libraries-for-both-lua5-2-and-5-1-using-luarocks

 3. Use `luarocks` to `install` `lsqlite3`. Make sure to use the luarocks
    version you compiled for Lua 5.1. This should create a file called
    `lsqlite3.so` that Minetest will try to find. If Minetest can not
    find this file, it will print out the folders that it was looking
    for. If that happens, try copying the `lsqlite3.so` file to one
    of the folders that Minetest prints out.

 4. Add `secure.trusted_mods` to the `minetest.conf` file and add `db`.
    If you plan on using the `irc` mod or other mods, you will need
    to add them here. Add them without spaces but with commas:
    `secure.trusted_mods = db,irc`

 5. You will need a `localmusic` mod. Copy the mod from inside the
    `.../games/insidethebox/mods/music/localmusic` to your
    `.../worlds/<worldname>/worldmods/` folder.

 6. Configure your admin account by setting `name = adminname` in the
    `minetest.conf` setting for this world. The admin gets a creative
    inventory access while players do not. There is no need to run the
    world in `creative` mode.

After that, you should be able to log in and start building a lobby,
and use the commands to create and play boxes.

Additional world settings you may need to change later are:

```

Optional world settings are:

```
lobby_respawn_center = (<x>,<y>,<z>) -- where to respawn players who come back
                                     -- to the lobby
lobby_respawn_radius = <min>,<max>   -- use this radius around the center

tutorial_required = <true,false>     -- are players required to complete this
tutorial_series = <id>               -- number of the tutorial series
tutorial_entry_lobby = <id>          -- tutorial entry lobby box_id
tutorial_exit_lobby = <id>           -- tutorial exit lobby box_id
tutorial_exit = (<x>,<y>,<z>)        -- where to teleport player at end of tutorial

announce_irc_admins = "john,frank"   -- no spaces, lists irc names that will receive admin announcements

```
