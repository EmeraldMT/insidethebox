
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

log = {}

function log.fs_data(player, name, formname, fields)
	assert(player or name)

	local pos = "(unknown pos)"
	if name and not player then
		player = minetest.get_player_by_name(name)
	end
	if player then
		pos = minetest.pos_to_string(vector.floor(player:get_pos()))
	end
	if not name and player then
		name = player:get_player_name()
	end
	local f = ""
	for k, _ in pairs(fields) do
		if f ~= "" then
			f = f .. ", "
		end
		f = f .. k
	end

	minetest.log("error", "invalid formspec data: player " .. name .. " at " ..
		pos .. " sends " .. formname .. ": [" .. f .. "]")
end

