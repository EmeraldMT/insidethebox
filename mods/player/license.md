`models/character.b3d`: sofar CC-BY-SA-4.0

`models/skin_jack.png`,
`textures/skin_preview_jack.png`,
`textures/gui_hotbar*`: From Isabella-II

`models/skin_jane.png`,
`models/skin_jean.png`,
`models/skin_jesse.png`,
`models/skin_jj.png`,
`models/skin_julio.png`,
`textures/skin_preview_jane.png`,
`textures/skin_preview_jean.png`,
`textures/skin_preview_jesse.png`,
`textures/skin_preview_jj.png`,
`textures/skin_preview_julio.png`: Assumed CC0, based on public posted skins, modified.

`textures/itb-logo.png`: sofar CC-BY-SA-4.0

`textures/bubble.png`,
`textures/heart.png`,
`textures/moon.png`,
`textures/sun.png`: From Isabella-II

`textures/skin_overlay_admin.png`: sofar CC-BY-SA-4.0

