
--[[

Copyright (c) 2013, Diego Martinez (kaeza)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--]]

if not irc then
	return
end

local qcache = { 0, 0 }

-- From irc/botcmds.lua
irc.register_bot_command("queue", {
	params = "",
	description = "Show review queue",
	func = function(user, args)
		local count = 0
		local now = os.time()
		if now > (qcache[1] + 60) then
			for id = 0, db.get_last_box_id() do
				local meta = db.box_get_meta(id)
				if (meta.type == db.BOX_TYPE and meta.meta) then
					if meta.meta.status == db.STATUS_SUBMITTED then
						count = count + 1
					end
				end
			end
			qcache[1] = now
			qcache[2] = count
		else
			count = qcache[2]
		end
		if count == 0 then
			return true, "There are no boxes in the review queue"
		elseif count == 1 then
			return true, "There is 1 boxes in the review queue"
		else
			return true, "There are " .. count .. " boxes in the review queue"
		end
	end,
})
