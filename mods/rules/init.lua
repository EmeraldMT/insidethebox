
--[[

Copyright (C) 2017 - Auke Kok <sofar@foo-projects.org>

"rules" is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1
of the license, or (at your option) any later version.

]]--

rules = {rules = {}}
local r = {}

r["player"] = [[

PLAYER RULES

-> Skip to the end for a FAQ!

-> To read the Creator rules, type: /rules creator

Content:
[1] Behavior
[2] Copyright
[3] Privacy Policy
[4] Cheating
[5] Hints
[6] Light
[7] Tutorial
[8] User generated content
[9] FAQ

[1] Behavior
Inside The Box is meant to be a pleasant place for all players, and
therefore we ask that all players behave accordingly. That means that
you should treat this server, the players on it, and those who may
come to experience the server through some other way with respect
and as you yourself would like to be treated.

It goes without saying that anything that players do that would alter,
disrupt or upset any part of the experience of players is a bannable
offense, without warning, and subject to being reported to authorities
if this is necessary. If you can't figure out what we mean by this,
please leave immediately.

[2] Copyright
Other than that, you should know that all the work on this server
is copyrighted, and that your ability to use or re-use any of
it is restricted and bound to certain conditions. Again, if you
don't know what this means and what you can or can not do, ask us
first. Of course, playing on this server is entirely allowed without
restrictions.

[3] Privacy Policy
You should know that we do monitor and log player conversations and
player interactions with the server. We do not provide these logs
to anyone unless legally required to do so, and we do not sell any
player information to other parties or companies. However, we will use
player data to analyze and improve the server experience for users,
so we may watch closely how players behave and interact on the server,
for instance by creating scores or rankings. If you do not agree with
this, please leave immediately.

[4] Cheating
Cheating, as described by using unfair means to gain an advantage
over other players, is expressly prohibited. That means that using
cheats to fly, look through walls, sprint, using noclip and unfairly
obtained other player capabilities while on this server are subject to
an immediate ban without warning. We believe that the best experience
is had when players experience the real challenge themselves without
unfair help. If you do discover a way to cheat or exploit, we'd love
to work with you to resolve this problem. If you don't tell us, but
abuse it or tell others, we may take action against you.

[5] Hints
We explicitly allow players to discuss and help players with
hints. This is not cheating. While we think that players get a
better experience trying to solve a problem and persist in that,
we realize that sometimes you just may not find that secret, well
hidden objective.

[6] Light
Because of the amount of available light in the game varies per
display and per computer, it is always allowed to pick up and move
torches. We know that certain levels are very dark and this may make
it very difficult for some players. Therefore we allow you to move
and place torches where ever you can. Of Course, not every level may
have torches available for the player to do so.

[7] Tutorial
All players are required to complete the tutorial before they are
allowed to enter the general lobby. We want to make sure the players
understand and appreciate the puzzle element of the game before they
start creating their own challenges.

[8] User generated content
We don't want to take over copyright of your works, but you can't build
on this server without somehow permitting the server to share your
creation with other players, and therefore, you are granting us the
rights to do so. Even if you never submit your creation for review,
you still grant the server whatever rights are applicable to share
any content you put in your own creations, and allow the server to
redistribute it to other people, without reservations.

[9] FAQ
Q: Can I record videos while playing on this server?
A: Yes, but, if you record with the music from this server, you
are required to provide the proper attribution, and you may not use
monetization or otherwise make videos for commercial purposes. The
credits are: "Lee Rosevere, Music inspired by MiNRS, CC-BY-NC-4.0"

Q: I think I found a bug!
A: contact sofar@foo-projects.org, or `sofar` in the game. Or visit
the website and visit the Feedback page.

Q: Where can I find more information?
A: Go to: minetest.foo-projects.org
]]

rules.rules["player"] = r.player:gsub("%]","\\]"):gsub("%[","\\["):gsub(";","\\;"):gsub(",","\\,"):gsub("\n", ",")

r.creator = [[

CREATOR RULES

These rules are for people who create puzzle boxes.

We want players to enjoy coming to this server, and to spend lots
of time playing puzzles. In order to promote this, we have a few
basic ground rules to prevent player frustration and to promote fun
and challenging puzzles.  Some of the things listed below are more
"guidelines" than hard rules, and some are really hard rules. If
you think that breaking a rule listed here would improve the game,
then discuss it with the admins, and be prepared to state your case.

1. The puzzle must always remain solvable.

An abandoned puzzle is a failed puzzle and a player lost. Do not ever
make a puzzle that a player *must* leave to reset. If your puzzle
needs some sort of reset button, you need to build it with the tools
available to you in the box creator mode.

2. Dying must never be required to complete a box.

A player must never feel like having to commit suicide. There should
always be a way out. If the player is actually in an unsolvable
situation, you should kill the player immediately, for instance by
flooding the player in lava.

3. Do not create "manholes" that the player can get stuck in.

A stuck player is an abandoning player. Either kill the player or
allow the player to recover.

4. Do not make darkness an essential part of your puzzle.

Your monitor may be nice and bright, but some people may have bad
vision or a darker monitor. And your cave system with 20 cave parts
that has 1 torch is just impossible to do without cheating and turning
up gamma on the monitor.  Dark boxes promote cheating and give an
unfair advantage. You can use darkness as an occasional feature,
or even anti-feature (e.g. making nonessential parts dark), but if
your box is mostly dark, it's not acceptable.

5. Do not use fake items.

Planting empty items or decoy items is the opposite of creativity,
and frustrates players. Try to reward the player instead. Unconnected
switches, empty chests, locked itemframes that are not in a "hint"
position or clearly decorative are all sources of frustration.

6. Do not make boxes that are 3/4 empty.

Empty large spaces feel boring and allow the player to quickly see
almost everything they need to find. Fill up the space, even up and
down, as much as you can do with decoration or playable areas.

7. When submitting, expect feedback, and update your box

Everyone wins if your boxes are better. You get a better box that will
be played more and where players are spending more time playing it.

8. Use buttons, not switches

Unless you need the player to flip a switch *two times*, you should
always use a button with a filter to open that door, create some node,
or open a pathway.

9. Don't build on the bottom.

This is a sure way to get yourself into trouble or make an
uninteresting and open box. Always throw some basic landscaping in
place with the landscaping tool and build at least a few blocks up.

10. Decorate, decorate, decorate

4 hours is almost the minimum of time you'll need to spend building to
make a decently decorated and well functioning puzzle, and more if you
add lots of mechanical components. You can not, I repeat, not make a
box in 30 minutes and expect it to be "well done". Even box 6, which
has a meager sub-100 nodes took several hours of work to get right.

Use the paint tool. Diversify your nodes. Use slabs to create relief
and texture. But don't overmix things either. 5 different color
flowers isn't stylish. Mixing certain blocks for building material
may also not match well.

11. Don't make a puzzle, first!

Always, always make the background and stage for your puzzle first.
It already is really hard to think of a good way to capture your
player and to tell a story, but you really can't do it unless there
is a clear atmosphere with a story present. The style of your box, the
areas that the player needs to visit, these all dictate the obstacles
that the player will have to overcome, and are easy puzzle elements
that you can add to make the puzzle more complex and more enjoyable.

12. Avoid signs and icons

Don't repeat the name of the box. Don't put your name somewhere
on a sign. These are all things that break the "4th wall" for players
and ruin the immersion feeling. A door bell on a normal house doesn't
have the word "doorbell" next to it. Icons similarly are not really
meant for decoration or hints, and usually distract more than anything.

13. Don't leave hints in your box

Don't use a terminal for player hints. They always figure it out,
and it breaks the 4th wall. Plus most of the time, the terminals
are a distraction. The terminals are for lore and stories you may
want to share. Try and stay away from personal stuff and overly
dramatic passages, though. It's a game, and should remain fun for
everyone.

14. Parkours rules

This server isn't meant for endless hardcore parkours. We will
accept boxes that are parkour focused but the emphasis should be
on puzzles and not jumps. On top of that, jumps must not be one
of these or further than these: 1 node up and over a 3 node gap;
across a level 4 node gap. 1 node down and over a 5 node gap; Any
jump should be reasonably doable. In doubt: if you can not repeat
the same jump 5x in succession without failing, it's too difficult.
]]

rules.rules["creator"] = r.creator:gsub("%]","\\]"):gsub("%[","\\["):gsub(";","\\;"):gsub(",","\\,"):gsub("\n", ",")

function rules.show(name, t)
	local f =
		"size[12,8]" ..
		"field_close_on_enter[input;false]" ..
		"textlist[0.4,0.5;11,6;rules;" ..
		rules.rules[t] ..
		"]" ..
		"button_exit[0.7,7;11.2,1;close;Close]"

	fsc.show(name, f, {}, function() end)
end

minetest.register_chatcommand("rules", {
	description = "Display server rules",
	func = function(name, param)
		if rules.rules[param] then
			rules.show(name, param)
		else
			rules.show(name, "player")
		end
	end
})

minetest.register_on_joinplayer(function(player)
	local name = player:get_player_name()
	minetest.after(15, function(n)
		if minetest.player_exists(n) then
			minetest.chat_send_player(n, "Remember to read the server rules! Type `/rules` to review them.")
		end
	end, name)
end)

