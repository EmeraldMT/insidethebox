unused_args = false
allow_defined_top = true
max_line_length = 130

read_globals = {
	"DIR_DELIM",
	"core",
	"dump",
	"VoxelManip", "VoxelArea",
	"PseudoRandom", "ItemStack",
	"SecureRandom",
	table = { fields = { "copy" } },
	string = { fields = { "split" } },
	"unpack",
	"AreaStore",
	"ranks",
	"irc"
}

globals = {
	"players", "db", "boxes", "mech", "doors", "fences", "screwdriver", "walls", "frame", "sfinv", "creative", "music", "skybox", "sounds", "conf", "log",
	"vector", "minetest",
}

